// console.log('hello')

//By convention, class names shd begin w/ an uppercase char
class Student {   // declaring a class;blueprint, plan, guidelines;no props
	// the constructor method defines how obj created fr this class will be assigned their initial prop values
	constructor(name, email, grades){
		this.name = name   // "this" refers to obj we create fr the class, not the class itself
		this.email = email
		this.gradeAve = undefined
		this.passed = undefined
		this.passedWithHonors = undefined

		// for 'grades'; array of 4 
		if(grades.length === 4) {
			if(grades.every(grade => grade >= 0 && grade <= 100)) {
				this.grades = grades
			}else {
				this.grades = undefined
			}
		}else {
			this.grades = undefined
		}
	}

	//methods must be outside of constructor
	login(){
		console.log(`${this.email} has logged in.`)
		return this
	}
	
	logout(){
		console.log(`${this.email} has logged out.`)
		return this
	}
	
	listGrades(){
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`)
		return this
	}

	computeAve(){
		let sum = 0
		this.grades.forEach(grade => sum = sum + grade)
		// return sum/4
		this.gradeAve = sum/4;
		return this  // --- double return not allowed
	}

	willPass(){
		// return this.computeAve() >= 85 ? true : false;
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this
	}

	willPassWithHonors(){
		if(this.computeAve().willPass().gradeAve >= 90){
			//return true
			this.passedWithHonors = true
		}else if(this.computeAve().willPass().gradeAve < 90 && this.willPass().computeAve().gradeAve >= 85){
			//return false
			this.passedWithHonors = false
		}else if(this.computeAve().willPass().gradeAve < 85){
			//return undefined
			this.passedWithHonors = undefined
		}
		return this
	}



}

// instantiate/create obj fr our Student class
// let studentOne = new Student("John", "john@mail.com",[89, 84, 78, 88])
// let studentTwo = new Student("Joe", "joe@mail.com",[78, 82, 79, 85])
// let studentThree = new Student("Jane", "jane@mail.com",[87, 89, 91, 93])
// let studentFour = new Student("Jessie", "jessie@mail.com",[91, 89, 92, 93])


//console.log(studentOne)

//The name given to a named class expression is local to the class's body. However, it can be 
//accessed via the name property.
// OUTPUT:
// Student.name
// 'Student'

// console.log(studentTwo)
// console.log(studentThree)
// console.log(studentFour)



class Section {
	constructor(name) {
		this.name = name
		this.students = []
		this.honorStudents = undefined
		this.honorsPercentage = undefined


	}
	
	addStudent(name, email, grades) {
		this.students.push(new Student(name, email, grades))
		return this
	}
	
	countHonorStudents() {
		let count = 0

		this.students.forEach(student => {
			if(student.willPassWithHonors().passedWithHonors) {
				count++
			}
		})
		this.honorStudents = count
		return this
	}
	computeHonorsPercentage() {
		let percentage = (this.countHonorStudents().honorStudents / this.students.length) * 100
		this.honorsPercentage = `${percentage}%`
		return this
	}
}



// console.log(studentOne)
// console.log(section1A)

// Section
// section1A.students[0]
// section1A.students[0].computeAve()
// section1A.students[0].computeAve().willPass()
// section1A.students[3].computeAve().willPass().willPassWithHonors()

class Grade {
	constructor(level) {
		this.level = level
		this.sections = []
		this.totalStudents = 0
		this.totalHonorStudents = 0
		this.batchAveGrage = undefined
		this.batchMinGrage = undefined
		this.batchMaxGrage = undefined

	}
	addSection(name) {
		this.sections.push(new Section(name))
		return this
	}
	//******************************************************
	countStudents() {
		let total = 0
		
		for (let i=0; i < this.sections.length; i++) {
			let ttlsec = this.sections[i]
			
			for (let j=0; j < this.ttlsec.length; i++) {
				total += this.ttlsec[j]
			}

		}
		this.totalStudents = total
		return this
	}

	countHonorStudents() {
		let count = 0

		this.honorStudents.forEach(honorStudent => {
			if(honorStudent.willPassWithHonors().passedWithHonors){
				count++
			}
		})

		this.totalHonorStudents = count
		return this
	}

	computeBatchAve() {
		let ttlave = 0

		
	}


}


let grade1 = new Grade(1)

grade1.addSection('section1A')
grade1.addSection('section1B')
grade1.addSection('section1C')
grade1.addSection('section1D')

let section1A = grade1.sections.find(section => section.name === "section1A")
let section1B = grade1.sections.find(section => section.name === "section1B")
let section1C = grade1.sections.find(section => section.name === "section1C")
let section1D = grade1.sections.find(section => section.name === "section1D")


section1A.addStudent("John", "john@mail.com",[89, 84, 78, 88])
section1A.addStudent("Joe", "joe@mail.com",[78, 82, 79, 85])
section1A.addStudent("Jane", "jane@mail.com",[87, 89, 91, 93])
section1A.addStudent("Jessie", "jessie@mail.com",[91, 89, 92, 93])

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

console.log(grade1)

//Define a countStudents() method that will iterate over every section in the grade level, incrementing the 
//totalStudents property of the grade level object for every student found in every section. -- count # of 
//studes  ==> 16


// }
//Define a countHonorStudents() method that will perform similarly to countStudents() except that it will 
//only consider honor students when incrementing the totalHonorStudents property.

//Define a computeBatchAve() method that will get the average of all the students' grade averages and divide 
//it by the total number of students in the grade level. The batchAveGrade property will be updated with the 
//result of this operation. ==> countstudes chain method

//Define a method named getBatchMinGrade() that will update the batchMinGrade property with the lowest grade 
//scored by a student of this grade level regardless of section.  ==> lowest single grade

//Define a method named getBatchMaxGrade() that will update the batchMaxGrade property with the highest grade 
//scored by a student of this grade level regardless of section.  ==> highest single grade